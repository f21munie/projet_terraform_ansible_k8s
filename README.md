# Descriptif du projet

- Auteur : Florian MUNIER
- Contact : florian.munier@imt-atlantique.net
- Formation : MS Infrastructures Cloud et DevOps
- Date : 23/01/2023 - 27/02/2023
- Description : Ce projet a pour but :
    - de mettre en place une infrastructure composée :
        - d'un réseau
        - d'un sous-réseau
        - d'un routeur
        - d'une paire de clés généréé automatiquement pour ce projet, copiée dans le répertoire projet "projet_terraform/.ssh/" 
        - d'une paire de clés généréé automatiquement pour les instances d'orchestration et copiées sur l'instance bastion dans le répertoire ".ssh"
        - d'une instance bastion
        - de 3 instances d'orchestration
        - d'une IP flottante admin
        - d'une IP flottante application
        - de 3 groupes de sécurité
        - d'un fichier "private_ips.txt" contenant les IP locales des instances, généré automatiquement et copié sur les instances
        - d'un fichier "public_ips.txt" contenant les IP publiques des instances "bastion" et "node01", généré automatiquement
        - d'un fichier "hosts.ini" et ssh.cfg servant à lancer des commandes pour le projet Ansible
    - de déployer l'application Vapormap automatiquement avec Ansible (par défaut sur l'instance Bastion) via docker-compose et sur un cluster Kubernetes

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/projet_terraform_ansible_k8s.git
```

#######################################################################################################
###################################CONFIGURER TERRAFORM###############################################
#######################################################################################################

# Installation de Terraform

- Installer Terraform :

```sh
TF_VERSION=1.3.6

echo "Downloading terraform binary ..."
if [ ! -e terraform_${TF_VERSION}_linux_amd64.zip ]; then  
  curl -O https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
fi
if [ ! -x ~/bin/terraform ]; then 
  unzip terraform_${TF_VERSION}_linux_amd64.zip -d ~/bin/
fi
rm -f terraform_${TF_VERSION}_linux_amd64.zip
```

- Vérifier le bon fonctionnement de Terraform :

```sh
terraform -v 
```

> Terraform v1.3.6
> on linux_amd64

# Configuration de l'interaction avec Openstack

Pour pouvoir interagir avec Openstack, créer ou récupérer via horizon un fichier RC, et initialiser l'environnement :

```sh
cd projet_terraform_ansible_k8s
source os-openrc.sh
env | grep OS_
```
> Le fichier "os-openrc.sh" correspond à votre fichier RC perso. N'oubliez pas de préciser le chemin.

# Renseigner les variables de Terraform

Il est nécessaire de paramétrer les variables suivantes afin de choisir vos caractéristiques de l'infrastructure (, nom du réseau, du sous-réseau, du routeur, le CIDR, ...). 
Pour cela, allez dans le fichier "main.tf" et veuillez à renseigner les variables. Vous pouvez modifier les variables à partir <ROUTER_NAME> jusqu'à <K3S_TOKEN>.

```sh
cd projet_terraform_ansible_k8s/Terraform
sudo vim main.tf
```

```sh
module "mod-os-private-network" {
  source                           = "./mod-os-private-network/"
  KEYPAIR_PATH                     = "$HOME/.ssh"
  EXTERNAL_NETWORK                 = "external"
  ROUTER_NAME                      = "router_tf_private"
  NETWORK_NAME                     = "network_tf_private"
  SUBNET_NAME                      = "subnet_tf_private"
  SUBNET_IP_RANGE                  = "192.168.1.0/24"
  DNS                              = ["192.44.75.10"]
  INSTANCE_BASTION_NAME            = "bastion"
  INSTANCE_BASTION_IMAGE           = "imta-docker"
  INSTANCE_BASTION_FLAVOR          = "s10.medium"
  INSTANCE_BASTION_KEY_PAIR        = "projet_terraform"
  INSTANCE_ORCHEST_NAME            = ["node01", "node02", "node03"]
  INSTANCE_ORCHEST_IMAGE           = "imta-docker"
  INSTANCE_ORCHEST_FLAVOR          = "s10.medium"
  INSTANCE_ORCHEST_KEY_PAIR        = "cluster_key"
  INSTANCE_ZABBIX_SERVER_NAME      = "zabbix-server"
  INSTANCE_ZABBIX_SERVER_IMAGE     = "imta-docker"
  INSTANCE_ZABBIX_SERVER_FLAVOR    = "s10.medium"
  INSTANCE_ZABBIX_SERVER_KEY_PAIR  = "projet_terraform"
  SECGROUP_BASTION_NAME            = "secgroup_bastion"
  SECGROUP_APPLICATION_NAME        = "secgroup_application"
  SECGROUP_INTERNAL_NETWORK_NAME   = "secgroup_internal_network"
  K3S_TOKEN                        = "cluster"
}
```

#######################################################################################################
#####################################CONFIGURER ANSIBLE################################################
#######################################################################################################

# Création d'un environnement virtuel

```sh
cd projet_terraform_ansible_k8s
python3 -m venv ./venv/ansible
source ./venv/ansible/bin/activate
pip install ansible
deactivate
```
#######################################################################################################
########################################DEPLOIEMENT###################################################
#######################################################################################################


# Déploiement de l'architecture et de l'application Vapormap

```sh
cd projet_terraform_ansible_k8s
source ./venv/ansible/bin/activate
cd projet_terraform_ansible_k8s/Terraform
terraform init
terraform plan
terraform apply
```
> Terraform apply vous demandera d'entrer une valeur (Enter a value:), taper "yes" et valider avec "entrée".

> L'application Vapormap est automatiquement déployée par Terraform qui lance le playbook Ansible.

# Accéder à l'application

Pour accéder à l'application Vapormap, rendez-vous sur les adresses suivantes :

> Vous pouvez trouver la variable <PUB_API_IP> dans le fichier "hosts.ini".

## Déploiement avec docker-compose

- Frontend

```sh
http://<PUB_API_IP>:8081
```

- API

```sh
http://<PUB_API_IP>:8082/api/points/
```

## Déploiement avec Kubernetes

- Frontend

```sh
http://<PUB_API_IP>
```

- API

```sh
http://<PUB_API_IP>/api/points/
```

# Visualisation de l'architecture

Vous pouvez vérifier l'état des ressources vues par Terraform. Elles seront listées avec leurs attribus.

```sh
terraform show
```
Vous pouvez également lister les ressources par type dans Openstack, directement en ligne de commande :

```sh
openstack network list 
openstack subnet  list
openstack router list 
openstack floating ip list
openstack security group list 
```

L'architecture est graphiquement visualisable via Openstack :

```sh
Projet / Réseau / Topologie du réseau
```

# Suppression de l'application Vapormap sur une instance

```sh
cd projet_terraform_ansible_k8s
source ./venv/ansible/bin/activate
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i Ansible/hosts.ini Ansible/destroy.yml
deactivate
```

# Suppression de l'architecture

Si vous voulez supprimer l'architecture, lancer la commande suivante :

```sh
cd projet_terraform_ansible_k8s/Terraform
terraform destroy
```
