# Création d'une paire de clés projet pour se connecter à bastion
resource "openstack_compute_keypair_v2" "keypair_project" {
  name = var.INSTANCE_BASTION_KEY_PAIR

  # Copie de la paire de clés sur la machine hébergeante
  provisioner "local-exec" {
    command = "mkdir ../.ssh; echo '${self.private_key}' > ../.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pem; echo '${self.public_key}' > ../.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pub; chmod 600 ../.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pem; chmod 600 ../.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pub"
  }
}

# Création d'une paire de clés pour le cluster
resource "openstack_compute_keypair_v2" "keypair_cluster" {
  name = var.INSTANCE_ORCHEST_KEY_PAIR

  # Copie de la paire de clés du cluster sur la machine hébergeante
  provisioner "local-exec" {
    command = "echo '${self.private_key}' > ../.ssh/${var.INSTANCE_ORCHEST_KEY_PAIR}.pem; echo '${self.public_key}' > ../.ssh/${var.INSTANCE_ORCHEST_KEY_PAIR}.pub; chmod 600 ../.ssh/${var.INSTANCE_ORCHEST_KEY_PAIR}.pem; chmod 600 ../.ssh/${var.INSTANCE_ORCHEST_KEY_PAIR}.pub"
  }

  depends_on = [openstack_compute_keypair_v2.keypair_project]
}

# Copie de la paire de clés du cluster sur l'instance bastion
resource "null_resource" "copy_keypair_cluster" {

  # Connexion à l'instance bastion
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${openstack_compute_keypair_v2.keypair_project.private_key}"
    host        = "${openstack_networking_floatingip_v2.floatip_admin.address}" 
    agent       = "false"
  }

  # Copie de la paire de clés du cluster sur bastion 
  # Ajout des droits
  provisioner "remote-exec" {
    inline = [
      "mkdir .ssh",
      "echo '${openstack_compute_keypair_v2.keypair_cluster.public_key}' > ${var.KEYPAIR_PATH}/${var.INSTANCE_ORCHEST_KEY_PAIR}.pub",
      "echo '${openstack_compute_keypair_v2.keypair_cluster.private_key}' > ${var.KEYPAIR_PATH}/${var.INSTANCE_ORCHEST_KEY_PAIR}.pem",
      "chmod 600 ${var.KEYPAIR_PATH}/${var.INSTANCE_ORCHEST_KEY_PAIR}.pub",
      "chmod 600 ${var.KEYPAIR_PATH}/${var.INSTANCE_ORCHEST_KEY_PAIR}.pem"]
  }

  depends_on = [openstack_compute_keypair_v2.keypair_project,
    openstack_compute_keypair_v2.keypair_cluster,
    openstack_compute_instance_v2.bastion_instance,
    openstack_compute_instance_v2.orchestration_instance]
}