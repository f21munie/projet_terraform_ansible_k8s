# Get datas from network external network
data "openstack_networking_network_v2" "ext_network" {
  name = var.EXTERNAL_NETWORK
}

# Create Router on Openstack
resource "openstack_networking_router_v2" "internal_router" {
  name                = var.ROUTER_NAME
  external_network_id = "${data.openstack_networking_network_v2.ext_network.id}"
}

resource "openstack_networking_router_interface_v2" "internal_router_interface_1" {
  router_id = "${openstack_networking_router_v2.internal_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.internal_subnet.id}"
}
